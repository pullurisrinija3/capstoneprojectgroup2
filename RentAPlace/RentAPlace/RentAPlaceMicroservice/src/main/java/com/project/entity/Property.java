package com.project.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Property {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int property_id;
	private String property_name;
	private String property_type;
	private String location;
	private String max_guest;
	private String address;
	private int rooms;
	private double room_size;
	private String property_line_content;
	@Column(length = 500)
	private String more_info;
	private int beds;
	private int price;
	private float ratings;
	private String image1;
	private String image2;
	private String image3;
	private String image4;
	private String image5;

	public Property() {
		
	}

	public Property(String property_name, String property_type, String location, String max_guest, String address,
			int rooms, double room_size, String property_line_content, String more_info, int beds, int price,
			float ratings, String image1, String image2, String image3, String image4, String image5) {
		super();
		this.property_name = property_name;
		this.property_type = property_type;
		this.location = location;
		this.max_guest = max_guest;
		this.address = address;
		this.rooms = rooms;
		this.room_size = room_size;
		this.property_line_content = property_line_content;
		this.more_info = more_info;
		this.beds = beds;
		this.price = price;
		this.ratings = ratings;
		this.image1 = image1;
		this.image2 = image2;
		this.image3 = image3;
		this.image4 = image4;
		this.image5 = image5;
	}


	public int getProperty_id() {
		return property_id;
	}
	
	public void setProperty_id(int property_id) {
		this.property_id = property_id;
	}

	public String getProperty_name() {
		return property_name;
	}

	public void setProperty_name(String property_name) {
		this.property_name = property_name;
	}
	
	public String getProperty_type() {
		return property_type;
	}

	public void setProperty_type(String property_type) {
		this.property_type = property_type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMax_guest() {
		return max_guest;
	}

	public void setMax_guest(String max_guest) {
		this.max_guest = max_guest;
	}

	public int getRooms() {
		return rooms;
	}

	public void setRooms(int rooms) {
		this.rooms = rooms;
	}

	public double getRoom_size() {
		return room_size;
	}

	public void setRoom_size(double room_size) {
		this.room_size = room_size;
	}

	public String getProperty_line_content() {
		return property_line_content;
	}

	public void setProperty_line_content(String property_line_content) {
		this.property_line_content = property_line_content;
	}

	public String getMore_info() {
		return more_info;
	}

	public void setMore_info(String more_info) {
		this.more_info = more_info;
	}

	public int getBeds() {
		return beds;
	}

	public void setBeds(int beds) {
		this.beds = beds;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public float getRatings() {
		return ratings;
	}

	public void setRatings(float ratings) {
		this.ratings = ratings;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getImage1() {
		return image1;
	}

	public void setImage1(String image1) {
		this.image1 = image1;
	}

	public String getImage2() {
		return image2;
	}

	public void setImage2(String image2) {
		this.image2 = image2;
	}

	public String getImage3() {
		return image3;
	}

	public void setImage3(String image3) {
		this.image3 = image3;
	}


	public String getImage4() {
		return image4;
	}

	public void setImage4(String image4) {
		this.image4 = image4;
	}

	public String getImage5() {
		return image5;
	}

	public void setImage5(String image5) {
		this.image5 = image5;
	}

	@Override
	public String toString() {
		return "Property [property_id=" + property_id + ", property_name=" + property_name + ", property_type="
				+ property_type + ", location=" + location + ", max_guest=" + max_guest + ", address=" + address
				+ ", rooms=" + rooms + ", room_size=" + room_size + ", property_line_content=" + property_line_content
				+ ", more_info=" + more_info + ", beds=" + beds + ", price=" + price + ", ratings=" + ratings
				+ ", image1=" + image1 + ", image2=" + image2 + ", image3=" + image3 + ", image4=" + image4
				+ ", image5=" + image5 + "]";
	}


	

	
	
	
	
	
	
	
}
