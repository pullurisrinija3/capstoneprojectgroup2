package com.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication(scanBasePackages = "com")
@EnableEurekaClient
public class RentAPlaceMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RentAPlaceMicroserviceApplication.class, args);
		System.out.println("server running on port number 8080");
	}
}
